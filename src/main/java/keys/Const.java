package keys;

/**
 * Created by Patryk Rutkowski on 27-06-2017.
 */
public class Const {

    public static final String USERNAME = "admin";
    public static final String PASSWORD = "admin";

    public static final String DATABASE_URL = "https://bpmn-f7319.firebaseio.com/";
    public static final String SERVER_URL = "http://52.35.98.93:8080/activiti-rest/service/";
    public static final String FIREBASE_KEY_PATH = "/serviceAccountKey.json";
    public static final String MAIN_WINDOW = "/main_window.fxml";
    public static final String LOGIN_WINDOW = "/login.fxml";
    public static final String WEATHER_WINDOW = "/weather.fxml";

    /**** FIREBASE ****/
    public static final String MAIN_DATABASE = "production";
    public static final String CHATROOMS_CHILD = "chatrooms";

    /**** CHAT ****/
    public static final String ADMIN_NAME = "Konsultant";
    public static final String USER_DISCONNECT_MESSAGE = "****************** Użytkownik się rozłączył ******************\n\n\n\n\n\n";
    public static final String ADMIN_DISCONNECT_MESSAGE = "****************** Konsultant się rozłączył ******************\n\n\n\n\n\n";

    /**** ACTIVITI ****/
    public static final String PROCESS_DEFINITION_ID = "process:26:7679";

    /**** ACTIVITI TASKS ****/
    public static final String OPTION_CHOICE = "wybor_opcji";
    public static final String WEATHER_SERVICE = "obsluga_prognozy_pogody";
    public static final String LOOKING_FOR_CONSULTANT = "wyszukiwania_wolnego_konsultanta";
    public static final String CONNECT_CLIENT_TO_CONSULTANT = "przypisanie_klienta_do_konsultanta";
    public static final String CALL_LATER = "propozycja_zadzwonienia_pozniej";
    public static final String INSERT_CONSULTANT_ID_AND_PASSWORD = "proba_o_podanie_identyfikatora_i_hasla_konsultanta";
    public static final String WAITING_FOR_CLIENT = "oczekiwanie_na_klienta";
    public static final String INSERT_CLIENT_ID = "proba_o_podanie_identyfikatora_klienta";
    public static final String CHAT_WITH_CONSULTANT = "rozmowa_z_konsultantem";
    public static final String END_CHAT = "zakonczenie_rozmowy";
    public static final String DISCONNECT = "rozlaczenie_z_systemem";

    /**** ACTIVITI FORM ****/
    public static final String OPTION_ID_LOG_FORM = "opcja";
    public static final String WEATHER_OPTION = "pogoda";
    public static final String CLIENT_OPTION = "logowanie_klient";
    public static final String CONSULTATNT_OPTION = "logowanie_konsultant";

    public static final String OPTION_ID_IS_CONSULTANT_FORM = "konsultant";
    public static final String CONSULTANT_OPTION_POSITIVE = "jest";
    public static final String CONSULTANT_OPTION_NAGATIVE = "nie_ma";
}
