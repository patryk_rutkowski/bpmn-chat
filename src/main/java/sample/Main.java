package sample;

import controllers.MainWindowController;
import controllers.LoginController;
import enums.UserType;
import interfaces.LoginListener;
import interfaces.MainWindowListener;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import keys.Const;
import models.Chatroom;
import models.Message;
import models.User;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseCredentials;
import com.google.firebase.database.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import models.apiModels.*;
import services.restServices.CallActivity;

import java.io.FileInputStream;
import java.util.Optional;

import static keys.Const.*;

public class Main extends Application implements LoginListener, MainWindowListener, EventHandler<WindowEvent> {

    private DatabaseReference connectedChatroomReference, chatroomsReference;
    private User user;
    private String chatroomKey = "";

    private MainWindowController mainWindowController;

    private UserType userType;

    private ProcessInstanceResponse processInstanceResponse;
    private WaitingTasksResponse tasksResponse;

    @Override
    public void start(Stage primaryStage) throws Exception {
        initRealtimeDatabase();
        ProcessInstanceRequest processInstanceRequest = new ProcessInstanceRequest(Const.PROCESS_DEFINITION_ID);
        processInstanceResponse = CallActivity.callToProcessInstance(processInstanceRequest);
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
        showMainWindow(primaryStage);
        showLoginDialog(primaryStage);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
    }

    public static void main(String[] args) {
        launch();
    }

    private void initRealtimeDatabase() throws Exception {
        FileInputStream serviceAccount = new FileInputStream("C:\\Users\\Adam Piech\\IdeaProjects\\bpmn-chat\\src\\main\\resources\\serviceAccountKey.json");
//        FileInputStream serviceAccount = new FileInputStream(getClass().getResource(FIREBASE_KEY_PATH).getPath());

        FirebaseOptions options = new FirebaseOptions.Builder()
                .setCredential(FirebaseCredentials.fromCertificate(serviceAccount))
                .setDatabaseUrl(DATABASE_URL)
                .build();

        FirebaseApp.initializeApp(options);

        chatroomsReference = FirebaseDatabase.getInstance().getReference(MAIN_DATABASE).child(CHATROOMS_CHILD);
    }

    private void showMainWindow(Stage primaryStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(MAIN_WINDOW));
        Parent root = fxmlLoader.load();

        mainWindowController = fxmlLoader.getController();

        primaryStage.setTitle("BPMN CALL CENTER");
        primaryStage.setScene(new Scene(root));
        primaryStage.setOnCloseRequest(this);
        primaryStage.show();
    }

    private void showLoginDialog(Stage ownerStage) throws Exception {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(LOGIN_WINDOW));
        Parent startDialog = fxmlLoader.load();

        LoginController loginController = fxmlLoader.getController();
        loginController.setListener(this);

        mainWindowController.setListener(this);
        Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.initOwner(ownerStage);
        dialog.setScene(new Scene(startDialog));
        dialog.setOnCloseRequest(this);
        dialog.show();
    }

    public void loginConsultant() {
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());

        user = new User();
        user.setNickname(ADMIN_NAME);
        mainWindowController.setData(user);
        userType = UserType.CONSULTANT;

        chatroomKey = chatroomsReference.push().getKey();
        connectedChatroomReference = chatroomsReference.child(chatroomKey);

        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());

        Chatroom newChatroom = new Chatroom();
        newChatroom.setAvailable(true);
        newChatroom.setId(chatroomKey);
        connectedChatroomReference.setValue(newChatroom);

        setChatroomReference();
    }

    public void loginUser(String nickname) {
        user = new User();
        user.setNickname(nickname);
        mainWindowController.setData(user);
        userType = UserType.USER;

        chatroomsReference.addListenerForSingleValueEvent(new ValueEventListener() {

            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    searchNewConsultant(dataSnapshot);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                setChatroomsListener();
            }

            public void onCancelled(DatabaseError databaseError) {}

        });
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
    }

    @Override
    public void onClickWeather() {
        CurrentFormRequest formRequest = new CurrentFormRequest();
        formRequest.setTaskId(tasksResponse.getData().get(0).getId());
        formRequest.addProperty(new Property(OPTION_ID_LOG_FORM, WEATHER_OPTION));
        CallActivity.callToCurrentForm(formRequest);
    }

    @Override
    public void checkWeather() {
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
    }

    @Override
    public void closeWeather() {
        tasksResponse.getData().get(0).getTaskDefinitionKey();
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
    }


    @Override
    public void onClickUserLogin() {
        CurrentFormRequest formRequest = new CurrentFormRequest();
        formRequest.setTaskId(tasksResponse.getData().get(0).getId());
        formRequest.addProperty(new Property(OPTION_ID_LOG_FORM, CLIENT_OPTION));
        CallActivity.callToCurrentForm(formRequest);
    }

    @Override
    public void onClickConsultantLogin() {
        CurrentFormRequest formRequest = new CurrentFormRequest();
        formRequest.setTaskId(tasksResponse.getData().get(0).getId());
        formRequest.addProperty(new Property(OPTION_ID_LOG_FORM, CONSULTATNT_OPTION));
        CallActivity.callToCurrentForm(formRequest);

        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
    }

    @Override
    public void onClickCancel() {
        CallActivity.callToRemoveProcess(processInstanceResponse.getId());
        ProcessInstanceRequest processInstanceRequest = new ProcessInstanceRequest(Const.PROCESS_DEFINITION_ID);
        processInstanceResponse = CallActivity.callToProcessInstance(processInstanceRequest);
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
    }

    /**
     * Sledzenie zmian w podłączonym chatroomie
     */
    private void setChatroomReference() {

        connectedChatroomReference.addValueEventListener(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                Chatroom chatroom = dataSnapshot.getValue(Chatroom.class);
                mainWindowController.newMessage(chatroom);
            }

            public void onCancelled(DatabaseError databaseError) {
            }

        });

    }

    public void sendMessage(final Message message, final boolean isRoomAvailable) {

        connectedChatroomReference.addListenerForSingleValueEvent(new ValueEventListener() {
            public void onDataChange(DataSnapshot dataSnapshot) {
                Chatroom chatroom = new Chatroom();
                if (userType == userType.CONSULTANT) {
                    chatroom.setAvailable((Boolean) dataSnapshot.child("available").getValue());
                } else {
                    chatroom.setAvailable(isRoomAvailable);
                }

                chatroom.setId(chatroomKey);

                chatroom.setMessage(message);
                connectedChatroomReference.setValue(chatroom);

            }

            public void onCancelled(DatabaseError databaseError) {}

        });

    }

    /**
     * Sledzenie zmian w gałęzi "chatrooms"
     */
    private void setChatroomsListener() {

        chatroomsReference.addValueEventListener(new ValueEventListener() {

            public void onDataChange(DataSnapshot dataSnapshot) {

                if (userType == UserType.CONSULTANT && (Boolean) dataSnapshot.child(chatroomKey).child("chatEnded").getValue() || chatroomKey.equals("")) {
                    try {
                        searchNewConsultant(dataSnapshot);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            public void onCancelled(DatabaseError databaseError) {}

        });

    }

    private boolean searchNewConsultant(DataSnapshot dataSnapshot) throws Exception {

        boolean connected = false;
        CurrentFormRequest formRequest = new CurrentFormRequest();
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));

        for (DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
            Boolean available = (Boolean) singleSnapshot.child("available").getValue();

            if (available) {
                CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
                tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));

                formRequest.setTaskId(tasksResponse.getData().get(0).getId());
                formRequest.addProperty(new Property(OPTION_ID_IS_CONSULTANT_FORM, CONSULTANT_OPTION_POSITIVE));
                CallActivity.callToCurrentForm(formRequest);

                chatroomKey = (String) singleSnapshot.child("id").getValue();
                connectedChatroomReference = chatroomsReference.child(chatroomKey);
                setChatroomReference();
                Thread.sleep(400);
                sendMessage(null, false);

                connected = true;
                break;
            }
        }

        if (connected) {
            CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
            tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
            CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
        } else {
            formRequest = new CurrentFormRequest();
            formRequest.setTaskId(tasksResponse.getData().get(0).getId());
            formRequest.addProperty(new Property(OPTION_ID_IS_CONSULTANT_FORM, CONSULTANT_OPTION_NAGATIVE));
            CallActivity.callToCurrentForm(formRequest);

            tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());

                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setHeaderText("Błąd");
                    alert.setContentText("Brak wolnego konsultanta");
                    final Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                        Platform.exit();
                    }
                }
            });


        }

        return connected;

    }

    private void adminEndedChat() {
        Chatroom chatroom = new Chatroom();
        chatroom.setChatEnded(true);
        chatroom.setAvailable(false);

        chatroom.setId(chatroomKey);

        Message message = new Message();
        message.setMessage(ADMIN_DISCONNECT_MESSAGE);
        user.setNickname("");
        message.setUser(user);

        chatroom.setMessage(message);
        connectedChatroomReference.setValue(chatroom);
        Platform.exit();

    }

    private void userEndedChat() {
        Chatroom chatroom = new Chatroom();
        chatroom.setChatEnded(false);
        chatroom.setAvailable(true);
        chatroom.setId(chatroomKey);

        Message message = new Message();
        message.setMessage(USER_DISCONNECT_MESSAGE);
        user.setNickname("");
        message.setUser(user);

        chatroom.setMessage(message);
        connectedChatroomReference.setValue(chatroom);
        Platform.exit();
    }

    public void restartApp() throws Exception {
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));

        switch (userType) {
            case USER:
                userEndedChat();
                CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
                break;
            case CONSULTANT:
                adminEndedChat();
                CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());

                break;
        }

        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
        tasksResponse = CallActivity.callToWaitingTasks(new WaitingTasksRequest(processInstanceResponse.getId()));
        CallActivity.callToCurrentTask(new CurrentTaskRequest(), tasksResponse.getData().get(0).getId());
        Platform.exit();
    }

    /**
     * Obsługa zamykania okna
     *
     * @param event
     */
    public void handle(WindowEvent event) {
        event.consume();
    }

}