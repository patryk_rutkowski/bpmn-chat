package interfaces;

/**
 * Created by patry on 28.06.2017.
 */
public interface WeatherWindowListener {

    void closeWindow();
    void checkWeather();

}
