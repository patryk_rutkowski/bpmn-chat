package interfaces;

import models.Message;

/**
 * Created by Patryk Rutkowski on 26.06.2017.
 */
public interface MainWindowListener {

    void sendMessage(Message message, boolean isRoomAvailable);
    void restartApp() throws Exception ;

}
