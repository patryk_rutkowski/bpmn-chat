package interfaces;

/**
 * Created by Patryk Rutkowski on 25.06.2017.
 */
public interface LoginListener {

    void loginConsultant();
    void loginUser(String nickname);
    void onClickWeather();
    void closeWeather();
    void checkWeather();
    void onClickUserLogin();
    void onClickConsultantLogin();
    void onClickCancel();

}
