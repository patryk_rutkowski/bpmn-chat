package controllers;

import interfaces.LoginListener;
import interfaces.WeatherWindowListener;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import services.restServices.CallActivity;

import static keys.Const.*;

/**
 * Created by Patryk Rutkowski on 25.06.2017.
 */
public class LoginController implements WeatherWindowListener {

    @FXML private Pane loginChoiceContainer;
    @FXML private Pane userLoginContainer;
    @FXML private Pane consultantLoginContainer;
    @FXML private TextField consultantLogin;
    @FXML private TextField consultantPassword;
    @FXML private TextField userLogin;

    private LoginListener loginListener;

    @FXML
    protected void onClickUserLoginScenario() {
        setActivePane(false, false, true);
        loginListener.onClickUserLogin();
    }

    @FXML
    protected void onClickConsultantLoginScenario() {
        setActivePane(false, true, false);
        loginListener.onClickConsultantLogin();
    }

    @FXML
    protected void onClickLoginConsultant() {
        if (consultantLogin.getText().equals(USERNAME) && consultantPassword.getText().equals(PASSWORD)) {
            if (loginListener != null) {
                loginListener.loginConsultant();
                ((Stage) userLoginContainer.getScene().getWindow()).close();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Błąd");
            alert.setContentText("Login lub hasło są błędne");
            alert.show();
        }
    }

    @FXML
    protected void onClickLoginUser() {
        if (loginListener != null) {
            loginListener.loginUser(userLogin.getText());
            ((Stage) userLoginContainer.getScene().getWindow()).close();
        }
    }

    @FXML
    protected void onClickCancel() {
        setActivePane(true, false, false);
    }

    @FXML
    protected void onClickExit() {
        Platform.exit();
    }

    @FXML
    protected void onClickWeather() throws Exception {
        loginListener.onClickWeather();
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(WEATHER_WINDOW));
        Parent startDialog = fxmlLoader.load();

        Stage dialog = new Stage();
        dialog.initModality(Modality.APPLICATION_MODAL);
        dialog.setScene(new Scene(startDialog));
        WeatherController weatherController = fxmlLoader.getController();
        weatherController.setListener(this);
        dialog.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                closeWindow();
            }
        });
        dialog.show();
    }

    private void setActivePane(boolean choice, boolean adminLogin, boolean userLogin) {
        loginChoiceContainer.setVisible(choice);
        userLoginContainer.setVisible(userLogin);
        consultantLoginContainer.setVisible(adminLogin);
    }

    public void setListener(LoginListener loginListener) {
        this.loginListener = loginListener;
    }

    @Override
    public void closeWindow() {
        loginListener.closeWeather();
    }

    @Override
    public void checkWeather() {
        loginListener.checkWeather();
    }
}
