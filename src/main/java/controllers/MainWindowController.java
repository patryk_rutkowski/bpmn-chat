package controllers;

import interfaces.MainWindowListener;
import models.Chatroom;
import models.Message;
import models.User;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class MainWindowController {

    @FXML private TextField sendText;
    @FXML private TextArea messages;

    private MainWindowListener mainWindowListener;
    private User user;

    @FXML
    protected void sendMessage() {
        if (!sendText.getText().equals("")) {
            if (mainWindowListener != null) {
                Message message = new Message();
                message.setMessage(sendText.getText());
                message.setUser(user);
                mainWindowListener.sendMessage(message, false);
            }
            sendText.setText("");
        }
    }

    @FXML
    protected void endChat() throws Exception {
        messages.setText("");
        sendText.setText("");
        mainWindowListener.restartApp();
    }

    public void setListener(MainWindowListener mainWindowListener) {
        this.mainWindowListener = mainWindowListener;
    }

    public void setData(User user) {
        this.user = user;
    }

    public void newMessage(Chatroom chatroom) {
        messages.setText(buildMessage(chatroom.getMessage()));
    }

    private String buildMessage(Message message) {
        StringBuilder stringBuilder = new StringBuilder(messages.getText());
        stringBuilder.append(message.getUser().getNickname());
        if (!message.getUser().getNickname().equals(""))
            stringBuilder.append(": ");
        stringBuilder.append(message.getMessage());
        stringBuilder.append('\n');
        return stringBuilder.toString();
    }

}
