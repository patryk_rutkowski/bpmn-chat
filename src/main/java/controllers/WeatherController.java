package controllers;

import interfaces.WeatherWindowListener;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import models.weather.Weather;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import services.restServices.WeatherService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * Created by Patryk Rutkowski on 27.06.2017.
 */
public class WeatherController {

    @FXML private TextField weatherLocalization;
    @FXML private Text weatherText;
    private WeatherService weatherService;
    private WeatherWindowListener weatherWindowListener;

    public WeatherController() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        weatherService = retrofit.create(WeatherService.class);
    }



    @FXML
    public void onClickCheckWeather() throws UnsupportedEncodingException {

        if (weatherLocalization.getText().isEmpty()) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setHeaderText("Błąd");
            alert.setContentText("Lokalizacja nie może być pusta");
            alert.show();
        } else {
            weatherService.getWeatherForLocalization(URLEncoder.encode(weatherLocalization.getText(), "UTF-8")).enqueue(new Callback<Weather>() {
                @Override
                public void onResponse(Call<Weather> call, Response<Weather> response) {
                    Platform.runLater(() -> weatherText.setText(prepareWeather(response.body())));
                    weatherWindowListener.checkWeather();
                }

                @Override
                public void onFailure(Call<Weather> call, Throwable throwable) {}

            });
        }

    }

    public void setListener(WeatherWindowListener weatherWindowListener) {
        this.weatherWindowListener = weatherWindowListener;
    }

    private String prepareWeather(Weather weather) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Wiatr: ");
        stringBuilder.append(weather.getWind().getSpeed()).append(" km/h").append("\n");
        stringBuilder.append("Poziom zachmurzenia: ").append(weather.getClouds().getAll()).append(" %").append("\n");
        stringBuilder.append("Temperatura: ").append(weather.getMain().getTemp()).append(" F").append("\n");
        return stringBuilder.toString();
    }

}
