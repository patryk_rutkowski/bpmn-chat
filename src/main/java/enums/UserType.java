package enums;

/**
 * Created by Patryk Rutkowski on 26.06.2017.
 */
public enum UserType {

    USER(0), CONSULTANT(1);

    private int id;

    UserType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
