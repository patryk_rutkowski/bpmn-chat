package models.apiModels;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adam Piech on 2017-06-28.
 */
public class CurrentFormRequest {

    private String taskId = null;
    private List<Property> properties= new ArrayList<>();

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public void addProperty(Property property) {
        this.properties.add(property);
    }
}
