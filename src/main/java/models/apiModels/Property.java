package models.apiModels;

/**
 * Created by Adam Piech on 2017-06-28.
 */
public class Property {

    private String id;
    private String value;

    public Property(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
