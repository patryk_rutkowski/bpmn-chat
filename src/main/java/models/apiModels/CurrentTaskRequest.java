package models.apiModels;

/**
 * Created by Adam Piech on 2017-06-27.
 */
public class CurrentTaskRequest {

    private String action = null;

    public CurrentTaskRequest() {
        this.action = "complete";
    }

    public CurrentTaskRequest(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

}
