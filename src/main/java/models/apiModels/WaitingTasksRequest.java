package models.apiModels;

/**
 * Created by Adam Piech on 2017-06-27.
 */
public class WaitingTasksRequest {

    private String executionId;

    public WaitingTasksRequest(String executionId) {
        this.executionId = executionId;
    }

    public String getExecutionId() {
        return executionId;
    }

    public void setExecutionId(String executionId) {
        this.executionId = executionId;
    }

}
