package models.apiModels;

import java.util.List;

/**
 * Created by Adam Piech on 2017-06-27.
 */
public class WaitingTasksResponse {

    List<WaitingTask> data;
    private int total;
    private int start;
    private int size;

    public List<WaitingTask> getData() {
        return data;
    }

    public void setData(List<WaitingTask> data) {
        this.data = data;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

}
