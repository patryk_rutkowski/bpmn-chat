package models.apiModels;

/**
 * Created by Adam Piech on 2017-06-27.
 */
public class ProcessInstanceRequest {

    private String processDefinitionId;

    public ProcessInstanceRequest(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

    public String getProcessDefinitionId() {
        return processDefinitionId;
    }

    public void setProcessDefinitionId(String processDefinitionId) {
        this.processDefinitionId = processDefinitionId;
    }

}
