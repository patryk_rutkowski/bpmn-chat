package models;

/**
 * Created by Patryk Rutkowski on 26.06.2017.
 */
public class Chatroom {

    private String id;
    private Message message;
    private boolean available;
    private boolean chatEnded;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public boolean isChatEnded() {
        return chatEnded;
    }

    public void setChatEnded(boolean chatEnded) {
        this.chatEnded = chatEnded;
    }
}
