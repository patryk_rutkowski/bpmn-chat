package models;

/**
 * Created by Patryk Rutkowski on 25.06.2017.
 */
public class User {

    private String nickname;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
}
