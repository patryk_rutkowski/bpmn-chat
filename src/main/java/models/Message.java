package models;

/**
 * Created by Patryk Rutkowski on 26.06.2017.
 */
public class Message {

    String message;
    User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
