package services.restServices;

import models.apiModels.*;
import retrofit2.Call;
import retrofit2.http.*;

/**
 * Created by Adam Piech on 2017-06-27.
 */
public interface API {

    @POST("runtime/process-instances")
    Call<ProcessInstanceResponse> getProcessInstance(@Body ProcessInstanceRequest body);

    @DELETE("runtime/process-instances/{processInstanceId}")
    Call<Void> removeProcessInstance(@Path("processInstanceId") String processInstanceId);

    @POST("query/tasks")
    Call<WaitingTasksResponse> getWaitingTasks(@Body WaitingTasksRequest body);

    @POST("runtime/tasks/{taskId}")
    Call<Void> executeTask(@Body CurrentTaskRequest body, @Path("taskId") String taskId);

//    @GET("form/form-data/{taskId}")
//    Call<TaskFormResponse> getTaskForm(@Path("taskId") String taskId);

    @POST("form/form-data")
    Call<Void> executeForm(@Body CurrentFormRequest body);

}
