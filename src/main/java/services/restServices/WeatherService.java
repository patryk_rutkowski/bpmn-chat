package services.restServices;

import models.weather.Weather;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Patryk Rutkowski on 27.06.2017.
 */
public interface WeatherService {

    //

    @GET("weather?appid=dc9c7318e09b386cbbe2fe05b1b715dc")
    Call<Weather> getWeatherForLocalization(@Query("q") String q);

}
