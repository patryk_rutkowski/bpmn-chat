package services.restServices;

import models.apiModels.*;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.IOException;

import static keys.Const.*;

/**
 * Created by Adam Piech on 2017-06-27.
 */
public class CallActivity {

    private static API api = initService();

    private static ProcessInstanceResponse processInstanceResponse = null;
    private static WaitingTasksResponse waitingTasksResponse = null;
    private static boolean isProcessRemoved = false;
    private static boolean isCurrentTaskExecuted = false;

    public static ProcessInstanceResponse callToProcessInstance(ProcessInstanceRequest body) {
        Call<ProcessInstanceResponse> processInstance = api.getProcessInstance(body);
        try {
            processInstanceResponse = processInstance.execute().body();
        } catch (IOException e) {}
        return processInstanceResponse;
    }

    public static boolean callToRemoveProcess(String processInstanceId) {
        Call<Void> call = api.removeProcessInstance(processInstanceId);
        try {
            isProcessRemoved = call.execute().isSuccessful();
        } catch (IOException e) {}
        return isProcessRemoved;
    }

    public static WaitingTasksResponse callToWaitingTasks(WaitingTasksRequest body) {
        Call<WaitingTasksResponse> waitingTasks = api.getWaitingTasks(body);
        try {
            waitingTasksResponse = waitingTasks.execute().body();
        } catch (IOException e) {}
        return waitingTasksResponse;
    }

    public static boolean callToCurrentTask(CurrentTaskRequest body, String taskId) {
        Call<Void> call = api.executeTask(body, taskId);
        try {
            isCurrentTaskExecuted = call.execute().isSuccessful();
        } catch (IOException e) {}
        return isCurrentTaskExecuted;
    }

    public static boolean callToCurrentForm(CurrentFormRequest body) {
        Call<Void> call = api.executeForm(body);
        try {
            isCurrentTaskExecuted = call.execute().isSuccessful();
        } catch (IOException e) {}
        return isCurrentTaskExecuted;
    }

    private static API initService() {
        return ServiceGenerator.createService(API.class, USERNAME, PASSWORD);
    }

}
